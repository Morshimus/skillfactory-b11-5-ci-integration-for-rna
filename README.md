# SkillFactory-B11-5-CI integration for  RNA

## [Vagrant test environment](https://github.com/Morshimus/SkillFactory-B11-5-CI-integration-for-rna-ENV)

## Задание

* [x] - **Требуется настроить пайплайн, который будет запускаться при изменении SQL-скрипта, хранящегося в репозитории (его выбор также на ваше усмотрение).**

> Сначала будет необходимо создать среду  в которой мы будем запускать Gitlab-Runner. Для этого была выбрана связка Vagrant+Vbox. Сделано с совместимостью на виндоус - файл actions.ps1 *(Активация файла . .\actions.ps1)*  
Сначала необоходимо склонировать репозиторий github по ссылке выше. Далее необходимо преднастроить среду.
Пожалуйста перед запуском комманд powershell сделайте настройку WSL2 по мануалу описанном [здесь](https://blog.thenets.org/how-to-run-vagrant-on-wsl-2/)

Также необходимо подкинуть в WSL ключ vagrant:

```powershell
wsl -d your_distr -u  your_account
```

```bash
cp your_path_to/.vagrant/machines/gitlab-runner-1/virtualbox/private_key ~/.ssh/vagrant_key && chmod 0600 ~/.ssh/vagrant_key
```

Старт среды.

```powershell
PS> . .\actions.ps1
PS> UpdateAnsibleRoles
PS> firewall -down
PS> vg -action up
```
<b><span style="color:red"> Важно! У вас должны быть права администратора на рабочей ОС.</span></b>

*После выполнения вернуть настройки фаерволла можно командой powershell => firewall -up*

* [x] - **Пайплайн должен запускать изменившийся скрипт и выводить результат запроса.**


![Video](video/1.mp4 "Video")

<details><summary> Скриншоты: </summary>

## Mysql-job :one:

![Job-1](img/1.jpg "Mysql job")

## Mysql-job :two:

![Job-2](img/2.jpg "Mysql job")

</details>

*PS В планах было добавить в Vagrant строчки triggres before - для удаления раннера с проекта, но стало лень :smile: . А вообще надо бы*

## [Gitlab-runner Ansible Role was used here](https://github.com/riemers/ansible-gitlab-runner)

## Powershell actions.ps1 functions for vagrant,firewall and updateansibleroles:
| function             | flag         | type         |action                                                           |
|----------------------|--------------|--------------|-----------------------------------------------------------------|
| vagrant              | -action      |  string      | vagrant second statement to do staff. [up,destroy,ssh]          |
| vagrant              | -provision   |  switch      | only works with up action - proceeding  provisioning.           |
| vagrant              | -force       |  switch      | Only works with destroy action - forcly destroy vm              |
| vagrant              | -user        |  string      | your wsl user to connect vm                                     |
| vagrant              | -distr       |  string      | your wsl distr name                                             |
| firewall             | -up          |  switch      | delete exclution for virtual WSL adapter from Sec profiles      |
| firewall             | -down        |  switch      | add exclustion for   virtual WSL adapter from Sec profiles      |
| UpdateAnsibleRoles   | ....         |  ......      | updating roles folder with roles in requirements.yml            |